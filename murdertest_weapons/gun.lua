function murdertest.weapons.gun_shoot(itemstack, player) -- No idea if it works really
	murdertest.weapons.update_ammo(itemstack, player)
	local def = itemstack:get_definition()
	if def["_murdertest_weapons:ammo"] == 0 then
		return murdertest.weapons.gun_reload(itemstack, player)
	end

	if def["_murdertest_weapons:can_fire"] == false then return false end
	def["_murdertest_weapons:can_fire"] = false
	def["_murdertest_weapons:ammo"] = def["_murdertest_weapons:ammo"] - 1
	murdertest.weapons.update_ammo(itemstack, player)
	local pos = player:get_pos()
	pos.y = pos.y + 1.65

	local dir = player:get_look_dir()
	local bullet = minetest.add_entity(pos, "murdertest_weapons:bullet", minetest.serialize{player = player:get_player_name()})
	if not bullet then return end
	bullet:set_pos(pos)
	bullet:set_rotation(player:get_look_dir())
	bullet:set_velocity{x = dir.x * 20, y = dir.y * 20, z = dir.z * 20}
	if def["_murdertest_weapons:ammo"] == 0 then
		murdertest.weapons.gun_reload(itemstack, player)
	else
		minetest.after(tonumber(murdertest.settings["gun.shot_delay"] or 1), function()
			if itemstack then
				def["_murdertest_weapons:can_fire"] = true
			end
		end)
	end
end

function murdertest.weapons.update_ammo(itemstack, player)
	local def = itemstack:get_definition()
	local ammo = def["_murdertest_weapons:ammo"]
	local max = tonumber(murdertest.settings["gun.max_ammo"] or 1)
	murdertest.set_indicator_text(player, "Ammo "..tostring(ammo).."/"..tostring(max))
end

function murdertest.weapons.gun_reload(itemstack, player)
	local def = itemstack:get_definition()
	if def["_murdertest_weapons:ammo"] ~= tonumber(murdertest.settings["gun.max_ammo"] or 1) and not def["_murdertest_weapons:reloading"] then
		def["_murdertest_weapons:reloading"] = true
		def["_murdertest_weapons:can_fire"] = false
		murdertest.set_indicator_text(player, "Reloading")
		minetest.after(tonumber(murdertest.settings["gun.reload_time"] or 3), function()
			if itemstack then
				def["_murdertest_weapons:ammo"] = tonumber(murdertest.settings["gun.max_ammo"] or 1)
				def["_murdertest_weapons:reloading"] = false
				def["_murdertest_weapons:can_fire"] = true
				murdertest.weapons.update_ammo(itemstack, player)
			end
		end)
		return true
	end
	return false
end
