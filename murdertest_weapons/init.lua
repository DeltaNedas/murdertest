print("Loading MurderTest Weapons module.")
dofile(minetest.get_modpath("murdertest_weapons").."/weapons.lua")

minetest.register_privilege("murdertest_drop_weapon", {
	description = "Lets you drop a Knife or Gun."
})

local knife = {}
knife.description = "Knife\nLeft Click to stab, Right Click then Left Click to throw."
knife.inventory_image = "knife.png"
knife.stack_max = 1

if murdertest.settings["knife.stabbing_enabled"] ~= "false" then
	knife.on_use = function(...) murdertest.weapons.knife_stab(...) end
else
	murdertest.print("Disabling knife stab.")
end
if murdertest.settings["knife.throwing_enabled"] ~= "false" then
	knife.on_place = function(...) murdertest.weapons.knife_throw(...) end
	knife.on_secondary_use = knife.on_place
else
	murdertest.print("Disabling knife throw.")
end

knife.on_drop = function(...) murdertest.weapons.weapon_drop(...) end

local gun = {}
gun.description = "Gun\nLeft Click to shoot, Right Click to reload."
gun.inventory_image = "gun.png"
gun.stack_max = 1
gun["_murdertest_weapons:ammo"] = tonumber(murdertest.settings["gun.max_ammo"] or 1)
gun["_murdertest_weapons:can_fire"] = true
gun["_murdertest_weapons:reloading"] = false

if murdertest.settings["gun.shooting_enabled"] ~= "false" then
	gun.on_use = function(...) murdertest.weapons.gun_shoot(...) end
else
	murdertest.print("Disabling gun shoot.")
end
if murdertest.settings["gun.reloading_enabled"] ~= "false" then
	gun.on_place = function(...) murdertest.weapons.gun_reload(...) end
	gun.on_secondary_use = gun.on_place
else
	murdertest.print("Disabling gun reload.")
end

gun.on_drop = function(...) murdertest.weapons.weapon_drop(...) end

minetest.register_craftitem("murdertest_weapons:knife", knife)
minetest.register_craftitem("murdertest_weapons:gun", gun)

local thrown_knife = {
	physical = true,
	collide_with_objects = false,
	visual = "sprite",
	visual_size = {x = 0.5, y = 0.125, z = 0.25},
	textures = {"thrown_knife.png"},
	type = "murdertest.weapons:thrown_knife",
	collisionbox = {-0.25, -0.0625, -0.125, 0.25, 0.0625, 0.125},
	on_activate = function(...) return murdertest.weapons.projectile_loaded(...) end,
	on_step = function(...) return murdertest.weapons.projectile_tick(...) end,
	get_staticdata = function(...) return murdertest.weapons.projectile_unloaded(...) end
}

local bullet = {
	physical = true,
	collide_with_objects = false,
	visual = "sprite",
	visual_size = {x = 0.25, y = 0.15},
	textures = {"bullet.png"},
	type = "murdertest.weapons:bullet",
	collisionbox = {-0.125, -0.075, -0.075, 0.125, 0.075, 0.075},
	on_activate = function(...) return murdertest.weapons.projectile_loaded(...) end,
	on_step = function(...) return murdertest.weapons.projectile_tick(...) end,
	on_punch = function(...) return murdertest.weapons.projectile_punched(...) end,
	get_staticdata = function(...) return murdertest.weapons.projectile_unloaded(...) end
}

local corpse = {
	physical = true,
	collide_with_objects = false,
	visual = "mesh",
	mesh = "character.b3d",
	textures = {"character_texture.png"},
	visual_size = {x = 1, y = 1},
	stepheight = 0.5, -- You can push it up half nodes i think
	infotext = "Corpse",
	on_activate = function(...) return murdertest.weapons.corpse_loaded(...) end,
	on_punch = function(...) return murdertest.weapons.corpse_punched(...) end,
	get_staticdata = function(...) return murdertest.weapons.corpse_unloaded(...) end
}

local collectible = {
	initial_properties = {
		textures = {""}
	},
	use_texture_alpha = true,
	physical = true,
	collide_with_objects = false,
	visual = "sprite",
	age = 0,
	visual_size = {x = 0.5, y = 0.5, z = 0.5},
	collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
	on_activate = function(...) return murdertest.weapons.collectible_loaded(...) end,
	on_step = function(...) return murdertest.weapons.collectible_tick(...) end,
	set_punish = function(self, player) return murdertest.weapons.collectible_set_punish(self, player) end,
	on_punch = function(...) return murdertest.weapons.collectible_punched(...) end,
	get_staticdata = function(...) return murdertest.weapons.collectible_unloaded(...) end
}

minetest.register_entity("murdertest_weapons:thrown_knife", thrown_knife)
minetest.register_entity("murdertest_weapons:bullet", bullet)
minetest.register_entity("murdertest_weapons:corpse", corpse)
minetest.register_entity("murdertest_weapons:collectible", collectible)
