function murdertest.weapons.knife_stab(itemstack, player, target)
	local meta = player:get_meta()
	
	if meta:get_string("murdertest_weapons:throwing") == "true" then
		murdertest.set_indicator_text(player)
		meta:set_string("murdertest_weapons:throwing", "false")
		local pos = player:get_pos()
		pos.y = pos.y + 1.65
		local dir = player:get_look_dir()
		local knife = minetest.add_entity(pos, "murdertest_weapons:thrown_knife", minetest.serialize{player = player:get_player_name()})
		if not knife then return end
		knife:set_pos(pos)
		knife:set_rotation(player:get_look_dir())
		local multiplier = tonumber(murdertest.settings["knife.throw_speed"] or 15)
		knife:set_velocity{x = dir.x * multiplier, y = dir.y * multiplier, z = dir.z * multiplier}
		knife:set_acceleration{x = dir.x * multiplier * -0.25, y = -9.810065, z = dir.z * multiplier * -0.25}
		return player:set_wielded_item(ItemStack("default:air"))
	elseif minetest.is_player(target) then -- Stab
		local player, _, _, player_game, player_game_data = murdertest.util.get_player_data(player)
		local target, _, _, target_game, target_game_data = murdertest.util.get_player_data(target)
		if player_game.name ~= target_game.name then return end
		if player_game.active ~= true or target_game.active ~= true then return end
		target:punch(player, nil, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = tonumber(murdertest.settings["knife.stab_damage"] or 20)},
		}, nil)
	end
end

function murdertest.weapons.knife_throw(itemstack, player)
	local meta = player:get_meta()
	local state = "false"
	murdertest.set_indicator_text(player, "")
	if meta:get_string("murdertest_weapons:throwing") == "false" then
		murdertest.set_indicator_text(player, "Throwing")
		state = "true"
	end
	meta:set_string("murdertest_weapons:throwing", state)
end
