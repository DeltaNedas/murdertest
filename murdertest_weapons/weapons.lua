murdertest.weapons = {}

function murdertest.set_indicator_text(player, text)
	local player, _, player_data = murdertest.util.get_player_data(player)
	text = text or ""
	local hud = player_data.hud
	hud.indicator.text = text
	hud:hide("indicator")
	if text ~= "" then
		minetest.after(0.016, function() hud:show("indicator") end)
		return true
	end
	return false
end

-- Load modules --
dofile(minetest.get_modpath("murdertest_weapons").."/knife.lua")
dofile(minetest.get_modpath("murdertest_weapons").."/gun.lua")
dofile(minetest.get_modpath("murdertest_weapons").."/projectile.lua")
dofile(minetest.get_modpath("murdertest_weapons").."/collectible.lua")

function murdertest.weapons.drop_weapon(player, punish)
	local player, player_name, _, _, player_data = murdertest.util.get_player_data(player)
	local weapon = player:get_wielded_item()
	local weapon_name = weapon:get_name()
	
	local inv = player:get_inventory()
	inv:remove_item("main", weapon)
	murdertest.set_indicator_text(player, "")

	local pos = player:get_pos()
	pos.y = pos.y + 1.65
	local dir = player:get_look_dir()

	local ammo
	if weapon_name == "murdertest_weapons:gun" then
		ammo = weapon:get_definition()["_murdertest_weapons:ammo"]
	end
	local item = minetest.add_entity(pos, "murdertest_weapons:collectible", minetest.serialize{item = weapon_name, player = player_name, punish = punish, ammo = ammo})
	if not item then return end
	item:set_rotation(dir)
	item:set_velocity{x = dir.x * 5, y = dir.y * 5, z = dir.z * 5}
	item:set_acceleration{x = 0, y = -9.810065, z = 0}

	if punish then
		minetest.after(tonumber(murdertest.settings["game.teamkill_blackout"] or 10), function()
			if item then
				item:set_punish()
			end
		end)
	end

	return true
end

function murdertest.weapons.punish(player, target, punish)
	local player, player_name, data, player_game, player_data = murdertest.util.get_player_data(player)
	local target, target_player_name, _, target_game, target_data = murdertest.util.get_player_data(target)
	if player_game.name ~= target_game.name then return false, 7 end
	if player_game.active ~= true or target_game.active ~= true then return false, 11 end
	local player_team = player_data.team
	local target_team = target_data.team
	local blackout = tonumber(murdertest.settings["game.teamkill_blackout"] or 10) > 0
	local drop_weapon = murdertest.settings["game.teamkill_drop_weapon"] ~= "false"
	if murdertest.is_teamkilling(player, target) then
		if blackout then
			local meta = player:get_meta()
			meta:set_string("murdertest_weapons:blacked_out", "true")
			local hud = data.hud
			hud:show(hud.team_reveal.background)
			minetest.after(tonumber(murdertest.settings["game.teamkill_blackout"] or 10), function()
				if meta then
					meta:set_string("murdertest_weapons:blacked_out", "false")
				end
			hud:hide(hud.team_reveal.background)
			end)
			return true
		elseif drop_weapon then
			return murdertest.drop_weapon(player, punish or true)
		end
	end
	return false, 12
end

function murdertest.weapons.weapon_drop(itemstack, player) -- Override Q, not when player dies
	if minetest.check_player_privs(player, {murdertest_drop_weapon = true}) then
		murdertest.weapons.drop_weapon(player, false)
		return itemstack:take_item(1)
	end
end
