function murdertest.weapons.corpse_loaded(self, staticdata)
	if staticdata and staticdata ~= "" then
		local data = minetest.deserialize(staticdata)
		self.player = data.player
		local _, _, _, _, player_game_data = murdertest.util.get_player_data(data.player)
		self.infotext = player_game_data.alias.." (Dead)"
	end
end

function murdertest.weapons.corpse_punched()
	-- Do physics stuff please
	return true
end

function murdertest.weapons.corpse_unloaded(self)
	local data = {}
	data.player = self.player
	return minetest.serialize(data)
end
