function murdertest.weapons.projectile_loaded(self, staticdata)
	if staticdata and staticdata ~= "" then
		local data = minetest.deserialize(staticdata)
		self.player = data.player
		self.punish = data.punish
	end
end

function murdertest.weapons.projectile_unloaded(self)
	local data = {}
	data.punish = self.punish
	data.player = self.player
	return minetest.serialize(data)
end

function murdertest.weapons.projectile_tick(self)
	if self.old_pos then
		self.object:set_rotation(vector.direction(self.old_pos, self.object:get_pos()))
	end

	local look_pos = self.object:get_pos()
	for i, v in pairs(self.object:get_rotation()) do
		look_pos[i] = look_pos[i] + v * 0.5
	end

	local node = minetest.get_node(look_pos)
	if node.name ~= "air" and node.name ~= "ignore" then
		local def = minetest.registered_nodes[node.name]
		if def.walkable then -- Solid blocks only!
			if self.type == "murdertest.weapons:thrown_knife" then
				local dir = self.object:get_rotation()
				local multiplier = tonumber(murdertest.settings["knife.throw_speed"] or 15)
				local item = murdertest.weapons.spawn_collectible(self, "murdertest_weapons:knife", tonumber(murdertest.settings["knife.auto_return"] or 30))
				item:set_acceleration{x = dir.x * multiplier * 0.05, y = -9.810065, z = dir.z * multiplier * 0.05}
				item:set_velocity{x = dir.x * multiplier * -0.25, y = dir.y * -0.25, z = dir.z * multiplier * -0.25}
			end
			self.object:remove()
			return self
		end
	end

	local objs = minetest.get_objects_inside_radius(self.object:get_pos(), 0.05)
	for _, obj in pairs(objs) do
		if minetest.is_player(obj:get_luaentity()) then
			if obj:get_luaentity():get_player_name() ~= self.player then
				print(object:get_lua_entity():get_player_name().." shot dead")
				local damage = tonumber(murdertest.settings["knife.throw_damage"] or 20)
				local auto_return = tonumber(murdertest.settings["knife.auto_return"] or 30)
				if self.type == "murdertest_weapons:bullet" then
					damage = tonumber(murdertest.settings["gun.shot_damage"] or 20)
				else
					if autoreturn <= 0 then autoreturn = nil end
					local item = murdertest.weapons.spawn_collectible(self, "murdertest_weapons:knife", auto_return)
					item:set_acceleration{x = 0, y = -9.810065, z = 0}
				end
				obj:punch(minetest.get_player_by_name(self.player), nil, {
					full_punch_interval = 1.0,
					damage_groups= {fleshy = damage},
				})
				minetest.sound_play("default_dig_cracky", {pos = self.object:get_pos(), gain = 0.8})
				self.object:remove()
				return self
			end
		end
	end
	self.old_pos = self.object:get_pos()
end

function murdertest.weapons.projectile_punched()
	return true
end
