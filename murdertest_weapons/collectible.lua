function murdertest.weapons.collectible_loaded(self, staticdata)
	if staticdata and staticdata ~= "" then
		local data = minetest.deserialize(staticdata)
		local props = self.object:get_properties()
		props.textures = {"dropped_"..data.item:match(":.+$"):sub(2)..".png"}
		self.object:set_properties(props)
		self.item = data.item
		self.punish = data.punish
		self.player = data.player
		self.ammo = data.ammo
	end
end

function murdertest.weapons.collectible_punched()
	return true
end

local min_collect_ticks = tonumber(murdertest.settings["game.collect_delay"] or 1) * 20

function murdertest.weapons.collectible_unloaded(self)
	local data = {}
	data.item = self.item
	data.punish = self.punish
	data.player = self.player
	data.ammo = self.ammo
	return minetest.serialize(data)
end

function murdertest.weapons.collectible_tick(self)
	if self.old_pos and self.age >= 20 and not self.stopped then
		if math.abs(vector.distance(self.old_pos, self.object:get_pos())) < 0.05 then
			self.stopped = true
			self.object:set_velocity{x = 0, y = 0, z = 0}
			self.object:set_acceleration{x = 0, y = -9.810065, z = 0}
		end
	end
	self.age = self.age + 1
	if self.age >= min_collect_ticks then
		local objs = minetest.get_objects_inside_radius(self.object:get_pos(), 0.5)
		for _, obj in pairs(objs) do
			if obj:get_player_name() ~= "" then
				if (self.punish and obj:get_player_name() == self.player) or not self.punish then
					local item = ItemStack(self.item)
					local inv = obj:get_inventory()
					local _, _, _, _, player_game_data = murdertest.util.get_player_data(obj)
					local team = player_game_data.team
					if self.item == "murdertest_weapons:gun" then
						if team == "bystander" then
							if murdertest.settings["gun.bystanders_pickup"] == "false" then
								return false
							end
						elseif murdertest.settings["gun.murderers_pickup"] ~= "true" then
							return false
						end
					elseif team == "murderer" then
						if murdertest.settings["knife.bystanders_pickup"] ~= "true" then
							return false
						end
					elseif murdertest.settings["knife.murderers_pickup"] == "false" then
						return false
					end
					if inv:contains_item("main", ItemStack("murdertest_weapons:knife")) or inv:contains_item("main", ItemStack("murdertest_weapons:gun")) then return end -- Dont pick up item twice
					inv:add_item("main", item)
					if self.item == "murdertest_weapons:gun" then
						item:get_definition()["murdertest_weapons:ammo"] = self.ammo
						murdertest.weapons.update_ammo(item, obj)
					end
					self.object:remove()
					return self
				end
			end
		end
	end
	self.old_pos = self.object:get_pos()
end

function murdertest.weapons.collectible_set_punish(self, player)
	self.punish = player
end

function murdertest.weapons.spawn_collectible(entity, name, auto_return)
	local item = minetest.add_entity(entity.object:get_pos(), "murdertest_weapons:collectible", minetest.serialize{item = name, player = entity.player, punish = entity.punish})
	if not item then return false end
	item:set_pos(entity.object:get_pos())
	item:set_rotation(entity.object:get_rotation())
	local player_name = entity.player
	if auto_return and auto_return > 0 then
		minetest.after(auto_return, function()
			if item then
				local player = minetest.get_player_by_name(player_name or "")
				if not player then return false end
				local inv = player:get_inventory()
				local itemstack = ItemStack(name)
				if inv:contains_item("main", itemstack) then return end
				inv:add_item("main", itemstack)
				item:remove()
				return item
			end
		end)
	end
	return item
end
