print("Loading MurderTest Item Wielding module.")
local wield = dofile(minetest.get_modpath("murdertest_wield").."/wield.lua")

local wielded_item = {
	initial_properties = {
		textures = {""}
	},
	physical = false,
	collisionbox = {-0.125, -0.125, -0.125, 0.125, 0.125, 0.125},
	visual_size = {x = 0.25, y = 0.25},
	visual = "wielditem",
	static_save = false,
	on_punch = function() return true end,
	on_activate = function(...) return wield.wielded_item_activated(...) end
}

minetest.register_entity("murdertest_wield:wielded_item", wielded_item)

if wield.settings["show_items"] ~= false then
	if wield.settings["use_whitelist"] == false then
		print("Showing all wielded items.")
		wield.loop()
	elseif wield.settings["invert_whitelist"] ~= true then
		print("Using item whitelist for wielded items.")
		wield.loop_whitelist()
	else
		print("Using item blacklist for wielded items.")
		wield.loop_blacklist()
	end
else
	print("Not showing wielded items.")
end
