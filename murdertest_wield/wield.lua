local wield = {}
wield.items = {
	["murdertest_weapons:knife"] = true,
	["murdertest_weapons:gun"] = true
}
local whitelist = wield.items
wield.settings = minetest.settings:to_table()

print("Ignore below")
if murdertest then
	murdertest.wield = wield
	murdertest.wield_settings = wield.settings
end

local wielding_players = {}
wield.wielding_players = wielding_players

local arm_position = {
	x = 0,
	y = 5.5,
	z = 3
}

local arm_rotation = {
	x = -90,
	y = 225,
	z = 90
}

wield.arm_position = arm_position
wield.arm_rotation = arm_rotation

local refresh_rate = wield.settings["refresh_rate"] or 0.016 -- 60 Hz
function wield.set_refresh_rate(rate)
	refresh_rate = rate
end

function wield.wielded_item_activated(self, staticdata)
	self.textures = {staticdata or "default:dirt"}
end

local function render_item(player, item)
	local name = player:get_player_name()
	local data = wielding_players[name]
	if not data then
		data = {}
	end
	if data.name ~= item then
		if item then
			item = item:get_name()
			local entity = minetest.add_entity(player:get_pos(), "murdertest_wield:wielded_item", item)
			if entity then
				entity:set_attach(player, "Arm_Right", arm_position, arm_rotation)
				data.entity = entity
				data.name = item
			end
		else
			if data.entity then
				data.entity:remove()
			end
			data = nil
		end
	end
	wielding_players[name] = data
	return true
end

local function loop()
	minetest.after(refresh_rate, loop)
	for _, player in pairs(minetest.get_connected_players()) do
		local item = player:get_wielded_item()
		if item:get_name() == "" then
			render_item(player)
		else
			render_item(player, item)
		end
	end
end
wield.loop = loop -- I think accessing local is faster than hashgame (wield)???

local function loop_whitelist()
	minetest.after(refresh_rate, loop_whitelist)
	for _, player in pairs(minetest.get_connected_players()) do
		local item = player:get_wielded_item()
		if item:get_name() == "" then
			render_item(player)
		elseif whitelist[item:get_name()] then
			render_item(player, item)
		end
	end
end
wield.loop_whitelist = loop_whitelist

local function loop_blacklist()
	minetest.after(refresh_rate, loop_blacklist)
	for _, player in pairs(minetest.get_connected_players()) do
		local item = player:get_wielded_item()
		if item:get_name() == "" then
			render_item(player)
		elseif not whitelist[item:get_name()] then
			render_item(player, item)
		end
	end
end
wield.loop_blacklist = loop_blacklist

return wield
