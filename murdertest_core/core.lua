murdertest = {}
murdertest.util = {}
murdertest.team = {}
murdertest.game = {}
murdertest.callbacks = {}

-- MurderTest map and stats format version
-- Increment if you change map or stats, then add a migration.
murdertest.version = 1

murdertest.commands = {}
murdertest.games = {}
murdertest.players = {}

murdertest.storage = minetest.get_mod_storage()
murdertest.settings = minetest.settings:to_table()

murdertest.chat_prefix = murdertest.settings["chat_prefix"] or "&cMT&7>&f "
murdertest.enable_colours = murdertest.settings["enable_colours"] or true

-- Defaults --

--[[
	local hud = murdertest.generate_hud("singleplayer")
	hud:show("team_reveal")
	hud:hide(hud.team_reveal.background)
		^
	notice the : and not .
	: calls it with hud then your parameters after
	you can also use hud.show(hud, "team_reveal"), etc. but its shorter
	murdertest.hud.show(hud, "whatever") should also work but its long
]]
murdertest.hud = {
	show = function(self, part)
		if type(part) == "string" then part = (self or {})[part] end
		if type(part) ~= "table" then return false end
		if part.hud_elem_type then
			if not part.id then
				part.id = self.player:hud_add(part)
			end
		else
			for _, def in pairs(part) do
				self:show(def)
			end
		end
	end,
	hide = function(self, part)
		if type(part) == "string" then part = (self or {})[part] end
		if type(part) ~= "table" then return false end
		if part.hud_elem_type then
			if part.id then
				self.player:hud_remove(part.id)
				part.id = nil
			end
		else
			for _, def in pairs(part) do
				self:show(def)
			end
		end
	end,
	player = nil,
	team = {
		hud_elem_type = "text",
		position = {x = 1, y = 1},
		offset = {x = -15, y = -65},
		text = "",
		name = "MurderTest Team",
		alignment = 1,
		scale = {x = 150, y = 50},
		number = 0xFFFFFF
	},
	victory = {
		background = {
			hud_elem_type = "image",
			position = {x = 0.5, y = 0.5},
			text = "grey.png",
			scale = {x = -80, y = -80}
		},
		winners = {
			hud_elem_type = "text",
			position = {x = 0.5, y = 0.20},
			offset = {x = 0, y = 0},
			text = "",
			name = "Winners",
			alignment = -1,
			scale = {x = -70, y = -10},
			number = 0xFFFFFF
		},
		players = {
			hud_elem_type = "text",
			position = {x = 0.5, y = 0.6},
			offset = {x = 0, y = 0},
			text = "",
			name = "Players",
			alignment = -1,
			scale = {x = -70, y = -60},
			number = 0xFFFFFF
		}
	},
	team_reveal = {
		background = {
			hud_elem_type = "image",
			position = {x = 0.5, y = 0.5},
			text = "black.png",
			scale = {x = -102, y = -102}
		},
		first_text = {
			hud_elem_type = "text",
			position = {x = 0.5, y = 0.25},
			offset = {x = 0, y = 0},
			text = "",
			name = "Team",
			alignment = 0,
			scale = {x = 800, y = 50},
			number = 0xFFFFFF
		},
		second_text = {
			hud_elem_type = "text",
			position = {x = 0.5, y = 0.25},
			offset = {x = 0, y = -35},
			text = "",
			name = "Gun",
			alignment = 0,
			scale = {x = 600, y = 25},
			number = 0x5533DD
		},
		third_text = {
			hud_elem_type = "text",
			position = {x = 0.5, y = 0.25},
			offset = {x = 0, y = 35},
			text = "",
			name = "Details",
			alignment = 0,
			scale = {x = 800, y = 25},
			number = 0xFFFFFF
		},
		fourth_text = {
			hud_elem_type = "text",
			position = {x = 0.5, y = 0.25},
			offset = {x = 0, y = 35},
			text = "",
				name = "Goal",
			alignment = 0,
			scale = {x = 500, y = 25},
			number = 0xFFFFFF
		}
	},
	alias = {
		hud_elem_type = "text",
		position = {x = 0, y = 1},
		offset = {x = 45, y = -65},
		text = "",
		name = "MurderTest Alias",
		alignment = 1,
		scale = {x = 150, y = 50},
		number = 0xFFFFFF
	},
	indicator = {
		hud_elem_type = "text",
		position = {x = 0, y = 1},
		offset = {x = 45, y = -125},
		text = "",
		name = "Weapon Indicator",
		alignment = 1,
		scale = {x = 150, y = 50},
		number = 0xFFFFFF
	}
}

murdertest.aliases = {
	"Alfa",
	"Bravo",
	"Charlie",
	"Delta",
	"Echo",
	"Foxtrot",
	"Golf",
	"Hotel",
	"India",
	"Juliett",
	"Kilo",
	"Lama",
	"Mike",
	"November",
	"Oscar",
	"Papa",
	"Quebec",
	"Romeo",
	"Sierra",
	"Tango",
	"Uniform",
	"Victor",
	"Whiskey",
	"Xray",
	"Zuru"
}

-- MurderTest functions --
dofile(minetest.get_modpath("murdertest_core").."/game.lua")
dofile(minetest.get_modpath("murdertest_core").."/team.lua")
dofile(minetest.get_modpath("murdertest_core").."/callbacks.lua")
dofile(minetest.get_modpath("murdertest_core").."/commands.lua")
dofile(minetest.get_modpath("murdertest_core").."/migrations.lua")

murdertest.team_inflate = {
	"both",
	"bystander",
	"murderer"
}

murdertest.team_deflate = {
	both = 1,
	bystander = 2,
	murderer = 3
}

function murdertest.load_games()
	local saved = minetest.deserialize(murdertest.storage:get_string("games"))
	if not saved then return {lobby = murdertest.create_game("lobby")} end
	local games = saved
	for name, game in pairs(games) do
		game.name = name
		game.version = game.v or murdertest.version
		game.v = nil
		game.spawnpoints = {}
		for spawn_name, spawn in pairs(game.s or {}) do
			game.spawnpoints[spawn_name] = {
				pos = spawn.p,
				dir = spawn.d,
				team = murdertest.team_inflate[spawn.t]
			}
			game.spawnpoints[spawn_name].team = team
		end
		game.s = nil
		murdertest.clean_game(game)
	end
	murdertest.games = games
	return murdertest.util.migrate("games")
end

function murdertest.save_games()
	local loaded = {}
	for name, game in pairs(murdertest.games) do
		loaded[name] = {
			s = {},
			v = game.version
		}
		for spawn_name, spawn in pairs(game.spawnpoints or {}) do
			loaded[name].s[spawn_name] = {
				p = spawn.pos,
				d = spawn.dir,
				team = murdertest.team_deflate[spawn.team]
			}
		end
	end
	loaded = minetest.serialize(loaded)
	return murdertest.storage:set_string("games", loaded)
end


function murdertest.player_killed(player, reason, killer)
	if murdertest.weapons then
		murdertest.weapons.set_indicator_text(player, "")
	end
	local _, player_name, player_data, game, game_data = murdertest.util.get_player_data(player_name)
	player_data.hud:hide("alias")
	if murdertest.util.is_teamkilling(killer, player) and game_data.team == "bystander" and murdertest.settings["game.announce_teamkills"] ~= "false" then
		local msg = player_name
		if murdertest.settings["game.alias_players"] ~= "false" then
			msg = msg.." ("..game_data.alias..")"
		end
		murdertest.message_game(game, murdertest.chat_prefix.."&4"..msg.." killed an innocent bystander.")
	elseif murdertest.stats and murdertest.settings["stats.track_deaths"] ~= "false" and not murdertest.util.is_teamkilling(killer, player) then
		murdertest.players[player_name].stats:increase("d", game_data.team)
		if killer then
			local _, killer_name, _, _, killer_game_data = murdertest.util.get_player_data(killer)
			if murdertest.settings["stats.track_kills"] ~= "false" then
				murdertest.stats[killer_name]:increase("k", killer_game_data.team)
			end
			killer_game_data.kills = killer_game_data.kills + 1
		end
	end
	return true
end

function murdertest.player_died(player, reason)
	if murdertest.weapons then
		murdertest.weapons.set_indicator_text(player, "")
	end
	if not reason.object then
		local _, player_name, player_data, game, game_data = murdertest.util.get_player_data(player_name)
		player_data.hud:hide("alias")
		if game_data.team == "murderer" then
			if game.murderers_alive == 1 then
				msg = "The murderer"
			elseif game_data.alias ~= player_name then
				msg = msg.." ("..game_data.alias..")"
			end
			murdertest.message_game(game, murdertest.chat_prefix.."&4"..msg.." died of mysterious circumstances.")
		end
		game_data.alive = false
		murdertest.update_game_players(game)
		if murdertest.stats and murdertest.settings["stats.track_deaths"] ~= "false" then
			murdertest.players[player_name].stats:increase("d", game_data.team)
		end
		return true
	end
	return false
end

function murdertest.after_start(player, game_data)
	murdertest.team.hide_reveal(player_name)
	murdertest.team.show_alias(player_name) -- works
	if tracking and murdertest.stats then
		murdertest.players[player_name].stats.increase("p", game_data.team)
	end
end


-- Main functions --

function murdertest.clean_game(game)
	if type(game) == "string" then game = murdertest.games[game] end
	if game.name ~= "lobby" then
		for i, _ in pairs(game.players or {}) do
			murdertest.game.join(i, "lobby")
		end
	end
	game.players = {}
	game.players_online = 0
	game.players_alive = 0
	game.bystanders_alive = 0
	game.murderers_alive = 0
	game.players_max = tonumber(murdertest.settings["game.players_max"] or 4)
	game.active = false
	game.starting = false
	game.time_played = 0
	return true
end

function murdertest.clean_games()
	for i, _ in pairs(murdertest.games) do
		murdertest.clean_game(i)
	end
	return true
end

function murdertest.list_games()
	local games = {}
	local game_indices = {}
	for name, game in pairs(murdertest.games) do
		if name ~= "lobby" and game.active == false and game.players_online < game.players_max then -- No Full/Active games
			local text = name.." "..tostring(game.players_online).."/"..tostring(game.players_max)
			if game.starting then
				text = text.." &e[Starting Soon]"
			end
			games[name] = text
			table.insert(game_indices, {name = name, online = game.players_online})
		end
	end

	table.sort(game_indices, function(a, b) return a.online > b.online end)
	return games, game_indices
end

function murdertest.generate_hud(player)
	local hud = murdertest.util.copy_table(murdertest.hud)
	hud.player = player
	return hud
end

function murdertest.generate_data(player)
	return {
		hud = murdertest.generate_hud(player)
	}
end

local function generate_alias(player)
	if type(player) ~= "string" then player = player:get_player_name() end
	return murdertest.aliases[math.random(1, #murdertest.aliases)]
end

function murdertest.generate_alias(player)
	if type(player) ~= "string" then player = player:get_player_name() end
	return player
end

if murdertest.settings["game.alias_players"] ~= "false" then
	murdertest.generate_alias = generate_alias
end


function murdertest.locate_game()
	local _, game_indices = murdertest.list_games()

	return (game_indices[1] or {}).name
end

function murdertest.say_raw(player_name, text)
	if type(player_name) ~= "string" then player_name = player_name:get_player_name() end
	local game = murdertest.players[player_name].game or murdertest.games.lobby
	local alias = player_name
	local player_alive = game.players[player_name].alive
	text = murdertest.format_colours(text) -- Not really "raw" but im lazy ok
	if game.active then alias = game.players[player_name].alias end
	for plr_name, plr in pairs(game.players) do
		if not (player_alive or plr.alive) or player_alive then -- dead players cant rat to alive ones, but *not* vice versa.
			minetest.chat_send_player(plr_name, text)
		end
	end
	return true
end

-- &0-f -> set text colour
-- &#0-f -> set background colour (( doesnt work, wait for minetest to let us use it outside of formspecs and stuff 0)
-- Try it out in chat like "&eHello!"
murdertest.escape_colours = { -- Minecraft style colours with bukkit (i think) escapes
	{"0", "000000"}, -- Black
	{"1", "0000AA"}, -- Blue
	{"2", "007F00"}, -- Green
	{"3", "00AAAA"}, -- Cyan
	{"4", "7F0000"}, -- Red
	{"5", "7F007F"}, -- Purple
	{"6", "FFA500"}, -- Orange
	{"7", "C0C0C0"}, -- Light Grey
	{"8", "7F7F7F"}, -- Grey
	{"9", "0000FF"}, -- Bright Blue
	{"a", "00FF00"}, -- Bright Green
	{"b", "00FFFF"}, -- Bright Cyan
	{"c", "FF0000"}, -- Bright Red
	{"d", "FF00FF"}, -- Pink
	{"e", "FFFF00"}, -- Bright Yellow
	{"f", "FFFFFF"}, -- White
}

function murdertest.format_colours(text)
	if murdertest.enable_colours then
		for _, colour in pairs(murdertest.escape_colours) do -- Remove all & and colour
			text = text:gsub("&"..colour[1], minetest.get_color_escape_sequence("#"..colour[2])) or text
			text = text:gsub("&#"..colour[1], minetest.get_background_escape_sequence("#"..colour[2])) or text
		end
	else
		text = text:gsub("&%e", "") or text -- Remove all & and dont colour
		text = text:gsub("&#%e", "") or text
	end
	return text
end

function murdertest.say(player_name, text)
	if type(player_name) ~= "string" then player_name = player_name:get_player_name() end
	text = murdertest.format_colours(text)
	return murdertest.say_raw(player_name, "&7<&3"..player_name.."&7>&f "..text)
end

function murdertest.message_game(game, text, ignore)
	if type(game) == "string" then game = murdertest.games[game] end
	for player_name, _ in pairs(game.players) do
		murdertest.message(player_name, text, ignore)
	end
	return true
end

function murdertest.join_message(player_name)
	local game = murdertest.players[player_name]
	if not game then return false, "Player is not in a game." end
	game = game.game
	local text = "&2Join>&f "..player_name
	if game.name ~= "lobby" then text = text.." ("..game.players_online.."/"..game.players_max..")" end
	return murdertest.message_game(game, text, true)
end

function murdertest.leave_message(player_name)
	local game = murdertest.players[player_name]
	if not game then return false, "Player is not in a game." end
	game = game.game
	local text = "&cQuit>&f "..player_name
	return murdertest.message_game(game, text, true)
end

function murdertest.update_game_players(game) -- Do census and check thresholds
	if type(game) == "string" then game = murdertest.games[game] end
	local online = 0
	local alive = 0
	local murderers_alive = 0
	local bystanders_alive = 0

	for _, player in pairs(game.players) do
		online = online + 1
		if player.alive then
			alive = alive + 1
			if player.team == "murderer" then
				murderers_alive = murderers_alive + 1
			else
				bystanders_alive = bystanders_alive + 1
			end
		end
	end

	game.players_online = online
	game.players_alive = alive
	game.murderers_alive = murderers_alive
	game.bystanders_alive = bystanders_alive

	if game.name ~= "lobby" then
		if game.active then
			if bystanders_alive == 0 and murdertest.settings["victory.bystanders_die"] ~= "false" then
				murdertest.game.finish(game, "murderer")
			elseif murderers_alive == 0 and murdertest.settings["victory.murderers_die"] ~= "false" then
				murdertest.game.finish(game, "bystander")
			end
		else
			if online >= (tonumber(murdertest.settings["game.start_threshold"]) or 4) then
				murdertest.game.start(game)
			else
				murdertest.game.abort(game)
			end
		end
	end
end

function murdertest.create_game(game_name)
	local game = {}
	game.players = {}
	game.players_online = 0
	game.players_alive = 0
	game.bystanders_alive = 0
	game.murderers_alive = 0
	game.bystanders = 0
	game.murderers = 0
	game.players_max = tonumber(murdertest.settings["game.players_max"] or 4)
	game.active = false
	game.starting = false
	game.time_played = 0
	game.spawnpoints = {{
		pos = {x = 0, y = 0, z = 0},
		dir = {h = 0, v = 0},
		team = "both"
	}}
	game.name = game_name
	game.version = murdertest.version

	if game_name == "lobby" then
		game.players_max = 0
	end

	return game
end

function murdertest.message(player, text, ignore)
	if type(player) ~= "string" then player = player:get_player_name() end
	text = tostring(text) or ""
	if not ignore then text = murdertest.chat_prefix..text end
	text = murdertest.format_colours(text)
	minetest.chat_send_player(player, text)
end

function murdertest.print(text, ...)
	return minetest.log("MurderTest> "..tostring(text).." "..table.concat({...}, "\t"))
end


function murdertest.help()
	local list = {}
	for command, _ in pairs(murdertest.commands) do
		table.insert(list, command)
	end
	return "Available commands: "..table.concat(list, ", ")
end

function murdertest.command(name, raw)
	raw = minetest.strip_colors(raw)
	murdertest.print("Player "..name.." ran command "..raw..".")
	local real = minetest.get_player_by_name(name) ~= nil
	local args = murdertest.util.split(raw) or {}
	if #args == 0 then
		if real then
			return murdertest.message(name, murdertest.help())
		else
			return murdertest.print(murdertest.help())
		end
	end
	local command_name = args[1]
	table.remove(args, 1)
	local command = murdertest.commands[command_name]
	if command then
		if real and command.real_player or not command.real_player then
			local ret, message = command.func(name, args, real)
			if message then
				if real then
					if not ret then
						message = "&4"..message
						message = message:gsub("\n", "\n&4") or message -- Colours dont apply after newlines idk why
					end
					return murdertest.message(name, message)
				else
					return murdertest.print(message)
				end
			end
			return true
		end
	elseif real then
		return murdertest.message(name, "Unknown command "..command_name..".")
	else
		return murdertest.print("Unknown command "..command_name..".")
	end
end

function murdertest.util.split(str, sep)
	str = tostring(str) or ""
	if str == "" then return end
	sep = tostring(sep or " ")
	local ret = {}
	for text in str:gmatch("[^%"..sep.."]+") do
		table.insert(ret, text)
	end
	return ret
end

function murdertest.util.compare(val, ...)
	for _, v in pairs{...} do
		if v == val then
			return true
		end
	end
	return false
end

function murdertest.util.thread(delay, func, a1, a2, a3)
	local f
	function f()
		pcall(function()
			a1, a2, a3 = func(a1, a2, a3) -- return all you pass
			minetest.after(delay, f)
		end)
	end
	return f
end

function murdertest.util.copy_table(t)
	local res = {}
	for i, v in pairs(t) do res[i] = v end
	return res
end

function murdertest.util.get_player_data(player)
	if type(player) == "string" then player = minetest.get_player_by_name(player) end
	local player_name = player:get_player_name()
	local player_data = murdertest.players[player_name]
	local player_game = player_data.game
	local player_game_data = player_game.players[player_name]
	return player, player_name, player_data, player_game, player_game_data
end

function murdertest.util.is_teamkilling(killer, player)
	local killer, killer_name, _, killer_game, killer_game_data = murdertest.util.get_player_data(killer)
	local player, player_name, _, player_game, player_game_data = murdertest.util.get_player_data(player)
	local bystander_teamkill = murdertest.settings["gun.friendly_fire"] ~= "false"
	local murdererer_teamkill = murdertest.settings["knife.friendly_fire"] == "true"
	if bystander_teamkill == false and murderer_teamill == false then return false, 5 end
	if killer_game.name ~= player_game.name then return false, 7 end
	if killer_game_data.alive ~= true or player_game_data.alive ~= true then return false, 8 end
	return (killer_game_data.team == "murderer" and player_game_data.team == "murderer" and murderer_teamkill) or (killer_game_data.team == "bystander" and player_game_data.team == "bystander" and bystander_teamkill)
end

function murdertest.util.migrate(stuff)
	for name, thing in pairs(murdertest[stuff] or {}) do
		if thing.version ~= murdertest.version then
			if thing.version or 1 > murdertest.version then return murdertest.print("MurderTest is out of date for "..tostring(stuff):sub(1, -2).." \""..name.."\". It requires version "..tostring(thing.version)..".") end
			murdertest.print("Migrating "..name.." from "..tostring(thing.version)..".")
			for i = thing.version, murdertest.version do
				local migration = murdertest.migrations[i]
				if migration then
					for old, new in pairs(migration) do
						local value = thing[old]
						if type(value) == "function" then
							local new, new_value = value(thing, old)
							thing[new] = new_value
						else
							thing[new] = value
						end
						thing[old] = nil
					end
				end
			end
		end
	end
end
