murdertest.migrations = {}
murdertest.migrations.games = {
	{} -- Version 1 has no migration
}
murdertest.migrations.stats = {
	{}
}
-- A migration is a table of:
-- - old keys (key) mapped to new keys (value)
-- - old keys (key) mapped to functions returning a new key and value (value)
-- A migration from a v1 map to v4 goes from 1 to 2 to 3 to 4, not 1 to 4.
-- This is so you don't need to manually migrate old migrations.
--[[ example:
[2] = { -- Migrate v2 to v3
	name = "map_name",
	spawnpoints = function(map, old_key) -- Make key less verbose
		local old_value = map.old_key
		return "spawns", {position = old_value.pos, direction = old_value.dir} -- Make value more verbose
	end
}
]]
