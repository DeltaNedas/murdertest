function murdertest.callbacks.player_joined(player)
	murdertest.players[player:get_player_name()] = murdertest.generate_data(player)
	if murdertest.stats and not murdertest.stats[player:get_player_name()] then
		murdertest.stats[player:get_player_name()] = murdertest.players[player:get_player_name()].stats.reset()
	end
	if murdertest.games.lobby then
		murdertest.game.join(player, "lobby")
	end
	return true
end

function murdertest.callbacks.player_left(player)
	if murdertest.games.lobby then
		murdertest.game.join(player, "lobby")
	end

	murdertest.leave_message(player)
	murdertest.players[player:get_player_name()] = nil
	return true
end

function murdertest.callbacks.player_chatted(player_name, text)
	if text:sub(1, 1) == "/" then
		return false
	end
	return murdertest.say(player_name, text)
end

function murdertest.callbacks.player_respawned(player)
	local player_name = player:get_player_name()
	local game = murdertest.players[player_name].game
	if game.name == "lobby" or not game.active then return false end
	murdertest.join(player, "lobby")
	return true
end

function murdertest.callbacks.player_punched(player, puncher, _, _, _, damage)
	local player, player_name, _, player_game, player_game_data = murdertest.util.get_player_data(player)
	local puncher, puncher_name, _, puncherr_game, puncher_game_data = murdertest.util.get_player_data(puncher)
	local bystander_teamkill = murdertest.settings["gun.friendly_fire"] ~= "false"
	local murderer_teamkill = murdertest.settings["knife.friendly_fire"] == "true"

	local player_bystander = player_game_data.team == "bystander"
	local puncher_bystander = puncher_game_data.team == "bystander"
	if (player_bystander and puncher_bystander and bystander_teamkill) or (not player_bystander and not puncher_bystander and murderer_teamkill) or (player_bystander ~= puncher_bystander) then
		player:set_hp(player:get_hp() - damage)
	end
	return true
end

function murdertest.callbacks.player_died(player, reason)
	local player, player_name, _, game, game_data = murdertest.util.get_player_data(player)
	if game and game.active then
		if murdertest.weapons then
			murdertest.weapons.drop_weapon(player)
		end
		local killer
		if reason.type == "punch" then
			killer = reason.object
			murdertest.player_killed(player, killer, reason)
		else
			murdertest.player_died(player, reason)
		end
	else
		return false, "Player not in game."
	end
end

function murdertest.callbacks.on_shutdown()
	murdertest.print("Saving games...")
	return murdertest.save_games()
end
