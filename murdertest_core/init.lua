print("Loading MurderTest Core module.")
dofile(minetest.get_modpath("murdertest_core").."/core.lua")

minetest.register_privilege("murdertest_reset_stats", {
	description = "Allows the use of /murdertest reset stats."
})

minetest.register_privilege("murdertest_reset_games", {
	description = "Allows the use of /murdertest reset games."
})

minetest.register_privilege("murdertest_reset_lobby", {
	description = "Allows the use of /murdertest reset lobby."
})

minetest.register_privilege("murdertest_set_game", {
	description = "Allows the use of /murdertest set <game name>."
})

minetest.register_privilege("murdertest_set_lobby", {
	description = "Allows the use of /murdertest set lobby."
})

minetest.register_privilege("murdertest_remove_game", {
	description = "Allows the use of /murdertest remove <game name>."
})

minetest.register_privilege("murdertest_start_game", {
	description = "Allows the use of /murdertest start."
})

minetest.register_chatcommand("murdertest", {
	params = "",
	description = "MurderTest master command, use it for help.",
	func = murdertest.command
})

minetest.register_on_joinplayer(function(...) return murdertest.callbacks.player_joined(...) end)
minetest.register_on_leaveplayer(function(...) return murdertest.callbacks.player_left(...) end)
minetest.register_on_chat_message(function(...) return murdertest.callbacks.player_chatted(...) end)
minetest.register_on_respawnplayer(function(...) return murdertest.callbacks.player_respawned(...) end)
minetest.register_on_punchplayer(function(...) return murdertest.callbacks.player_punched(...) end)
minetest.register_on_dieplayer(function(...) return murdertest.callbacks.player_died(...) end)
minetest.register_on_shutdown(function() return murdertest.callbacks.on_shutdown() end)

murdertest.load_games()
