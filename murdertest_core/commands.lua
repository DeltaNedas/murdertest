murdertest.commands.help = {
	help = function()
		return
[[
Usage: help [command]
Prints usage of command or lists all available commands for MurderTest.
What usage means:
	string: required as is
	<value>: a value is required
	[value]: a value is optional
]]
	end,
	func = function(name, args, real)
		local ret = murdertest.help()
		if #args > 0 then
			if murdertest.commands[args[1] ] then
				ret = murdertest.commands[args[1] ].help()
			end
		end
		return true, ret
	end
}

murdertest.commands.set = {
	help = function()
		return
[[
Usage: set <game> [number] [bystander/murderer]
Sets a game's spawnpoint.
If game is lobby then it sets the lobby return point, which is 0, 0, 0 by default.
Number is a non-0, positive integer, defaulting to 1.
If team is specified then the spawnpoint is restricted to a team.
If multiple spawnpoints are set then a player will spawn at a random one.
Requires the murdertest_set_lobby or murdertest_set_game privileges.
]]
	end,
	func = function(name, args, real)
		if not real then return false, "Please run set a real player." end
		local player = minetest.get_player_by_name(name)
		if #args == 0 then return false, murdertest.commands.set.help() end
		local game_name = args[1]
		local number = math.floor(tonumber(args[2]) or 1)
		if number < 1 then number = 1 end

		local team = args[3] or "both"
		if murdertest.util.compare(team, "murderer", "m") then
			team = "murderer"
		elseif murdertest.util.compare(team, "bystander", "b") then
			team = "bystander"
		elseif team ~= "both" then
			return false, "Please specify a valid team."
		end

		if game_name == "lobby" then
			if not minetest.check_player_privs(name, {murdertest_set_lobby = true}) then
				return false, "Check your privilege, you need murdertest_set_lobby."
			end
		else
			if not minetest.check_player_privs(name, {murdertest_set_game = true}) then
				return false, "Check your privilege, you need murdertest_set_game."
			end
		end
		local game = murdertest.games[game_name] or murdertest.create_game(game_name)
		game.spawnpoints[number] = {
			pos = player:get_pos(),
			dir = {h = player:get_look_horizontal(), v = player:get_look_vertical()},
			team = team
		}
		murdertest.games[game_name] = game
		if not murdertest.players[name].game then
			murdertest.game.join(name, game_name)
		end
		return true, murdertest.message(name, "Game spawnpoint #"..tostring(number).." set to "..minetest.pos_to_string(player:get_pos())..".")
	end
}

murdertest.commands.reset = {
	help = function()
		return
[[
Usage: reset <stats/games/lobby/>
Resets all player stats, games or lobby points.
Requires respective murdertest_reset_* privilege for each.
]]
	end,
	func = function(name, args, real)
		if not real then return false, "Please run reset a real player." end
		if #args == 0 then return false, murdertest.commands.reset.help() end
		local resetting = table.concat(args, " ")
		if murdertest.util.compare(resetting, "stats", "games", "lobby") then
			if not minetest.check_player_privs(name, {["murdertest_reset_"..resetting] = true}) then
				return false, "Check your privilege, you need murdertest_reset_"..resetting.."."
			end
			if resetting == "lobby" then
				murdertest.games.lobby = murdertest.create_game("lobby")
				return true, "The lobby has been reset to 0,0,0."
			elseif resetting == "games" then
				local lobby = murdertest.games.lobby
				murdertest.games = {}
				murdertest.games.lobby = lobby
			elseif murdertest.stats then
				for _, stats in pairs(murdertest.stats) do
					murdertest.default_stats.reset(stats) -- Works because not using :
				end
			end
			return true, "All "..resetting.." have been reset."
		else
			return false, murdertest.commands.reset.help()
		end
	end
}

murdertest.commands.join = {
	help = function()
		return
[[
Usage: join <game>
Attempts to join a game.]]
	end,
	func = function(name, args, real)
		if not real then return false, "Please run join a real player." end
		if #args == 0 then return false, murdertest.commands.join.help() end
		local game_name = table.concat(args, " ")
		murdertest.print("Player joined "..game_name..".")
		local msg = "Joined game "..game_name.."."
		if game_name == "lobby" then
			msg = "Teleported you to the lobby."
		end
		
		local success, message = murdertest.game.join(name, game_name)
		if not success then msg = "Error occured while joining "..game_name..": "..message end
		return success, msg
	end
}

murdertest.commands.lobby = {
	help = function()
		return
[[
Usage: lobby
Brings you back to the lobby.
]]
	end,
	func = function(name, _, real)
		return murdertest.commands.join.func(name, {"lobby"}, real)
	end
}

murdertest.commands.list = {
	help = function()
		return
[[
Usage: list [top]
Lists most populated, not yet started games.
If top is specified only the top X games will be shown.
Top defaults to 10.]]
	end,
	func = function(name, args, real)
		local top = args[1] or 10
		local games, game_indices = murdertest.list_games()
		if top > #game_indices then top = #game_indices end
		murdertest.message(name, "Top "..tostring(top).." most populated games:")
		for i = 1, top do
			local v = game_indices[i]
			local game = games[v.name]
			if real then
				murdertest.message(name, "#"..tostring(i)..": "..game)
			else
				murdertest.print("#"..tostring(i)..": "..game)
			end
		end
		return true
	end
}

murdertest.commands.auto = {
	help = function()
		return
[[
Usage: auto
Automatically joins the #1 game from /murdertest list.
]]
	end,
	func = function(name, _, real)
		local game = murdertest.locate_game()
		if not game then
			return false, "No games found."
		end
		return murdertest.commands.join.func(name, {game}, real)
	end
}

murdertest.commands.remove = {
	help = function()
		return
[[
Usage: remove <game> [number]
Removes a game, or the nth spawnpoint.
Requires the murdertest_remove_game privilege.
]]
	end,
	func = function(name, args, real)
		if not real then return false, "Please run set a real player." end
		if not minetest.check_player_privs(name, {murdertest_remove_game = true}) then
			return false, "Check your privilege, you need murdertest_remove_game."
		end
		local player = minetest.get_player_by_name(name)
		if #args == 0 then return false, murdertest.commands.remove.help() end
		local game_name = args[1]
		local game = murdertest.games[game_name]
		if not game then return false, "No game found." end

		if args[2] then
			local number = math.floor(tonumber(args[2]))
			if number < 1 then number = 1 end
			game.spawnpoints[number] = nil
			return true, "Removed spawnpoint #"..tostring(number).." in "..game.name.."."
		end
		murdertest.clean_game(game_name)
		murdertest.games[game_name] = nil
		return true, "Removed game "..game_name.."."
	end
}

murdertest.commands.start = {
	help = function()
		return
[[
Usage: start
Starts the game you are currently in.
Requires the murdertest_start_game privilege.
]]
	end,
	func = function(name, _, real)
		if not real then return false, "Please run set a real player." end
		if not minetest.check_player_privs(name, {murdertest_start_game = true}) then
			return false, "Check your privilege, you need murdertest_start_game."
		end
		local player, player_name, _, map = murdertest.util.get_player_data(name)
		if map.active then return false, "Map already started." end
		murdertest.game.start(map)
	end
}
