function murdertest.team.show_reveal(player)
	local _, _, player_data = murdertest.util.get_player_data(player)
	return player_data.hud:show("team_reveal")
end

function murdertest.team.hide_reveal(player)
	local _, _, player_data = murdertest.util.get_player_data(player)
	return player_data.hud:hide("team_reveal")
end

function murdertest.team.show_stats(player)
	local _, _, player_data = murdertest.util.get_player_data(player)
	return player_data.hud:show("stats")
end

function murdertest.team.hide_stats(player)
	local _, _, player_data = murdertest.util.get_player_data(player)
	return player_data.hud:hide("stats")
end

function murdertest.team.show_alias(player)
	local _, _, player_data = murdertest.util.get_player_data(player)
	return player_data.hud:show("alias")
end

function murdertest.team.hide_alias(player)
	local _, _, player_data = murdertest.util.get_player_data(player)
	return player_data.hud:hide("alias")
end

function murdertest.team.join(player, team, armed, murderers, all_players)
	local player, _, player_data, _, game_data = murdertest.util.get_player_data(player)

	local hud = player_data.hud
	print(player, "joined "..team)

	if team == "murderer" then
		-- bloody scream sound TODO
		print("BLOODY SCREAAAM")
		game_data.team = "murderer"
		hud.team.text = "Murderer"
		hud.team.number = 0xDD2222
		if murderers == 1 then
			hud.team_reveal.first_text.text = "You are the murderer"
		else
			hud.team_reveal.first_text.text = "You are a murderer"
		end
		hud.team_reveal.first_text.number = 0xDD2222
		print("Armed:", armed)
		if armed then
			local inv = player:get_inventory()
			print("Knife:", inv:add_item("main", ItemStack("murdertest_weapons:knife")))
		else
			hud.team_reveal.second_text.text = "with no weapon"
			hud.team_reveal.second_text.number = 0xDD2222
		end
		if murderers == 1 or all_players == murderers then -- 1 or all are murderers
			hud.team_reveal.third_text.text = "Kill everyone"
		else -- teamwork
			hud.team_reveal.third_text.text = "Kill all bystanders"
		end
		hud.team_reveal.third_text.number = 0xDD2222
		hud.team_reveal.fourth_text.text = "Don't get caught"
		hud.team_reveal.fourth_text.number = 0xDD2222
	elseif armed then
		print("player armed")
		hud = murdertest.team.join(player, "bystander", false, murderers, all_players)
		local inv = player:get_inventory()
		inv:add_item("main", ItemStack("murdertest_weapons:gun"))
		if murdertest.weapons then
			murdertest.weapons.update_ammo(ItemStack("murdertest_weapons:gun"), player)
		end
		hud.team_reveal.second_text.text = "with a secret weapon"
		hud.team_reveal.second_text.number = 0x5533DD
		if murderers == 1 then
			hud.team_reveal.fourth_text.text = "Find and kill him"
		elseif murderers > 1 then
			hud.team_reveal.fourth_text.text = "Find and kill them"
		end
	else
		game_data.team = "bystander"

		hud.team.text = "Bystander" -- remove residual info + massive grammar heiling
		hud.team.number = 0x3333DD
		if all_players - murderers == 1 then
			hud.team_reveal.first_text.text = "You are the bystander"
		else
			hud.team_reveal.first_text.text = "You are a bystander"
		end
		hud.team_reveal.first_text.number = 0x3333DD
		hud.team_reveal.second_text.text = ""
		if murderers == 0 then
			hud.team_reveal.third_text.text = "There are no murderers"
		elseif murderers == 1 then
			hud.team_reveal.third_text.text = "There is a murderer on the loose"
		else
			hud.team_reveal.third_text.text = "There are murderers on the loose"
		end
		hud.team_reveal.third_text.number = 0x3333DD
		hud.team_reveal.fourth_text.text = "Don't get killed"
		hud.team_reveal.fourth_text.number = 0x3333DD
	end
end

function murdertest.team.pick(game)
	local armed = tonumber(murdertest.settings["gun.amount_spawned"] or 1)
	local murderers = 0
	if armed > game.players_online then
		armed = game.players_online
	else
		murderers = game.players_online - armed
	end
	murdertest.print("armed:", armed)
	murdertest.print("murderers:", murderers)

	local available = {}
	for player_name, data in pairs(game.players) do
		table.insert(available, player_name)
		local player, player_name, player_data, player_game, player_game_data = murdertest.util.get_player_data(player_name)
		local inv = player:get_inventory()
		inv:set_list("main", {})

		murdertest.team.hide_stats(player)

		player_game_data.alias = murdertest.generate_alias(player)
		player:set_properties{infotext = player_game_data.alias}
		local hud = player_data.hud
		if murdertest.settings["game.alias_players"] ~= "false" then
			hud.alias.text = player_game_data.alias -- works surprisingly
			hud:show("alias")
		else
			hud.alias.text = ""
			hud:hide("alias")
		end
	end

	print("WWWWWW")
	for i = 1, armed do -- give guns
		local index = math.random(1, #available)
		local player = minetest.get_player_by_name(available[index])
		table.remove(available, index)
		print("armed bystander")
		murdertest.team.join(player, "bystander", true, murderers, game.players_online)
		print("no")
	end
	print("picked weapon")
	print("knife is ", murderers)
	for i = 1, murderers do -- give knives
		print("fuck off")
		local index = math.random(1, #available)
		local player = minetest.get_player_by_name(available[index])
		table.remove(available, index)
		print("made murderer")
		murdertest.team.join(player, "murderer", true, murderers, game.players_online)
	end

	for i = 1, game.players_online - armed - murderers do -- unarmed bystanders
		local index = math.random(1, #available)
		print("bystander")
		local player = minetest.get_player_by_name(available[index])
		table.remove(available, index)
		murdertest.team.join(player, "bystander", false, murderers, game.players_online)
	end
	minetest.after(0.01, function()
		for name, _ in pairs(game.players) do
			murdertest.team.show_reveal(name)
			murdertest.game.teleport(name)
		end
	end)

	return true
end
