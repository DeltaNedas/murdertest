function murdertest.game.teleport(player)
	local player, _, _, game, game_data = murdertest.util.get_player_data(player)
	local available_spawns = {}
	for _, spawn in pairs(game.spawnpoints) do
		if spawn.team == "both" or spawn.team == game_data.team then
			table.insert(available_spawns, spawn)
		end
	end

	if #available_spawns == 0 then return false, "No available spawns in game." end
	local spawn = available_spawns[math.random(1, #available_spawns)]
	player:set_pos(spawn.pos)
	player:set_look_horizontal(spawn.dir.h)
	player:set_look_vertical(spawn.dir.v)
	return true
end

function murdertest.game.start_countdown(game, seconds)
	if type(seconds) ~= "number" then return end
	if type(game) == "string" then game = murdertest.games[game] end
	if not game.starting then error("aborted") return end
	seconds = seconds - 1
	if seconds < 1 then
		print("it started")
		murdertest.team.pick(game)
		game.starting = false
		game.active = true
		murdertest.message_game(game, "Game has started!") -- not shown
		local tracking = murdertest.settings["stats.track_plays"] ~= "false"
		minetest.after(5, function()
			for player_name, game_data in pairs(game.players) do
				murdertest.after_start(player_name, game_data)
			end
		end)
		error("started")
	end

	if seconds < 6 then -- Countdown last 5 seconds
		local countdown_message = "Game starting in 1 second."
		if seconds > 1 then
			countdown_message = "Game starting in "..tostring(seconds).." seconds."
		end
		murdertest.message_game(game, countdown_message)
	end
	
	return game, seconds
end

function murdertest.game.start(game)
	if type(game) == "string" then game = murdertest.games[game] end
	game.starting = true
	local starting_message = "Game has started!"
	local time = tonumber(murdertest.settings["game.start_countdown"] or 20)
	if time == 1 then
		starting_message = "Game starting in 1 second."
	elseif time > 1 then
		starting_message = "Game starting in "..tostring(time).." seconds."
	end
	murdertest.message_game(game, starting_message)
	murdertest.util.thread(1, murdertest.game.start_countdown, game, time)()
end

function murdertest.game.finish(game, team)
	if type(game) == "string" then game = murdertest.games[game] end
	if game.name == "lobby" then return false end

	local winners = ""
	if team == "bystander" then
		if game.bystanders_alive == 1 then
			winners = "Bystander wins!"
		else
			winners = "Bystanders win!"
		end
	elseif team == "murderer" then
		if game.murderers_alive == 1 then
			winners = "Murderer wins!"
		else
			winners = "Murderers win!"
		end
	else
		winners = "Nobody won."
	end

	local players = ""
	for player_name, data in pairs(game.players) do
		if not data.alive then players = players.."(Dead) " end
		players = players..data.team.."s: "..player_name
		if data.alias ~= player_name then players = players.." ("..data.alias..")" end
		if data.kills > 0 then
			players = players..": "..tostring(data.kills).." Kills"
		end
		players = players.."\n"
	end
	if players:sub(-1) == "\n" then players = players:sub(1, -2) end

	for player_name, data in pairs(game.players) do
		if data.team == team and murdertest.settings["stats.track_wins"] ~= "false" then
			murdertest.players[player_name].stats.increase("w", team)
		end
		murdertest.players[player_name].hud.victory.winners = winners
		murdertest.players[player_name].hud.victory.players = players
		murdertest.players[player_name].hud:show("victory")
		minetest.after(tonumber(murdertest.settings["game.lobby_time"] or 5), function()
			if murdertest.players[player_name] then
				murdertest.players[player_name].hud:hide("victory")
				murdertest.join(player_name, "lobby")
			end
		end)
	end
end

function murdertest.game.abort(game)
	if type(game) == "string" then game = murdertest.games[game] end
	if game.active then return false, "Game already started." end
	if not game.starting then return false, "Game not starting." end
	game.starting = false
	return murdertest.message_game(game, "Game start aborted.")
end

function murdertest.game.join(player_name, game_name)
	local player
	if type(player_name) == "string" then player = minetest.get_player_by_name(player_name) else player = player_name; player_name = player:get_player_name() end
	if not game_name then return false, "Provide a game to join." end
	if not player then return false, "Player not found." end
	if not murdertest.games[game_name] then return false, "Game not found." end
	local game = murdertest.games[game_name]
	if (game.players_online == game.players_max) and game.players_max ~= 0 then return false, "Game is full." end
	if game.active then return false, "Game is active." end
	if game.players[player:get_player_name()] and game.name ~= "lobby" then return false, "You are already in that game." end
	if game_name == "lobby" and murdertest.settings["game.auto_join"] ~= "false" then -- If you join lobby it will auto join most full game
		local top = murdertest.locate_game()
		if top then
			return murdertest.game.join(player_name, top)
		end
	end
	game.players[player_name] = {
		name = player_name,
		kills = 0,
		alive = true,
		team = "",
		alias = murdertest.generate_alias(player_name)
	}
	local old_game = murdertest.players[player_name].game
	if game.active then return false, "Your game is active!" end
	murdertest.players[player_name].game = game

	murdertest.print(player_name.." joined game "..game.name..".")
	if old_game and old_game.name ~= "lobby" then
		murdertest.print("Kicking "..player_name.." out of old game, "..old_game.name..".")
		old_game.players[player_name] = nil
		murdertest.update_game_players(old_game)
		murdertest.leave_message(player_name)
	end
	
	murdertest.update_game_players(game)
	if game_name ~= "lobby" then
		murdertest.join_message(player_name)
	end
	if murdertest.hud.stats then
		murdertest.players[player_name].hud:show("stats")
	end
	
	return murdertest.game.teleport(player)
end
