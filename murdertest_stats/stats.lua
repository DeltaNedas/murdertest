local compare = murdertest.util.compare
murdertest.stats = {}
murdertest.default_stats = {
	plays = {
		bystander = 0,
		murderer = 0
	},
	wins = {
		bystander = 0,
		murderer = 0
	},
	kills = {
		bystander = 0,
		murderer = 0
	},
	deaths = {
		bystander = 0,
		murderer = 0
	},
	reset = function(self)
		local stats = murdertest.stats[self.player]
		for _, v in pairs(stats) do
			if type(v) == "table" then
				for i, _ in pairs(v) do
					v[i] = 0
				end
			end
		end
		stats:refresh(self.player)
		return stats
	end,
	increase = function(stat, team)
		stat = stat_deflation[stat]
		if not stat then return false end
		team = team_deflation[team]
		if not team then return false end

		local stats = murdertest.stats[player_name]
		stats[stat][team] = stats[stat][team] + 1
		murdertest.print("Increased "..player_name.."'s "..stat_inflation[stat].." stat as "..team_inflation[team]..".") -- Log it

		murdertest.refresh_stats(player_name)
	end
}

function murdertest.sortcache(amount, mode, team, fairness)
	return {{name = "DeltaNedas", value = 0}, {name = "placeholder", value = 0}} --temp until i make it
end

murdertest.commands.top = {
	help = function()
		return
[[
Usage: top <plays/wins/kills/deaths> [bystander/murderer]
Returns the top 5 MurderTest players sorted by overall <mode>
Bystander and murderer are kills of either team.
You can use the first letter for each option.]]
	end,
	func = function(name, args)
		if #args == 0 then
			return murdertest.message(name, murdertest.commands.top.help())
		end
		local mode = args[1]
		local team = args[2]
		local result = "Top 5 MurderTest players sorted by"

		if compare(team, "bystander", "b") then
			team = true
			result = result.." bystander"
		elseif compare(team, "murderer", "m") then
			team = false
			result = result.." murderer"
		else
			return murdertest.message(name, "Invalid team.")
		end

		if compare(mode, "plays", "p") then
			result = result.." games played:"
		elseif compare(mode, "wins", "w") then
			result = result.." games won:"
			mode = "wins"
		elseif compare(mode, "kills", "k") then
			result = result.." kills:"
			mode = "kills"
		elseif compare(mode, "deaths", "d") then
			result = result.." deaths:"
			mode = "deaths"
		else
			return murdertest.message(name, "Invalid mode.")
		end

		-- #1 | yaboy: 50 deaths

		local top = murdertest.sortcache(5, mode, team)
		for i, v in pairs(top) do
			result = result.."\n#"..tostring(i).." | "..v.name..": "..v.value.." "..mode
		end
		return murdertest.message(name, result)
	end
}
