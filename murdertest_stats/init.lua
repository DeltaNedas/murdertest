print("Loading MurderTest Stats module.")
dofile(minetest.get_modpath("murdertest_stats").."/stats.lua")
dofile(minetest.get_modpath("murdertest_stats").."/migrations.lua")

-- Saved:
-- k = {b = 0, m = 0}
-- Loaded:
-- kills = {bystander = 0, murderer = 0}
-- Its compressed when saved to save disk space
-- Ideally it would always be compressed to save disk and ram, but :get() and :set(val) < [] we all know this
function murdertest.load_stats()
	local saved = minetest.deserialize(murdertest.storage:get_string("stats"))
	if not saved then return {} end
	for name, stats in pairs(saved) do
		stats.player = name
		stats.plays = {bystander = stats.p.b, murderer = stats.p.m}
		stats.p = nil
		stats.wins = {bystander = stats.w.b, murderer = stats.w.m}
		stats.w = nil
		stats.kills = {bystander = stats.k.b, murderer = stats.k.m}
		stats.k = nil
		stats.deaths = {bystander = stats.d.b, murderer = stats.d.m}
		stats.d = nil
		stats.version = stats.v or 1
		stats.v = nil
	end
	murdertest.stats = saved
	return murdertest.util.migrate("stats")
end

function murdertest.save_stats()
	if murdertest["stats.save_stats"] ~= "false" then
		local loaded = {}
		for name, stats in pairs(murdertest.stats) do
			loaded[name] = stats
			stats.player = nil
			stats.p = {b = stats.plays.bystander, m = stats.plays.murderer}
			stats.plays = nil
			stats.w = {b = stats.wins.bystander, m = stats.wins.murderer}
			stats.wins = nil
			stats.k = {b = stats.kills.bystander, m = stats.kills.murderer}
			stats.kills = nil
			stats.d = {b = stats.deaths.bystander, m = stats.deaths.murderer}
			stats.deaths = nil
			stats.v = stats.version or murdertest.version
			stats.version = nil
			for i, v in pairs(stats) do
				if type(v) == "function" then stats[i] = nil end
			end
		end
		loaded = minetest.serialize(loaded)
		return murdertest.storage:set_string("stats", loaded)
	end
	return false
end

local old_generate_data = murdertest.generate_data
local old_on_shutdown = murdertest.callbacks.on_shutdown

local stat_deflation = {
	plays = "p", p = "p",
	won = "w", w = "w",
	kills = "k", k = "k",
	deaths = "d", d = "d"
}

local stat_inflation = { -- not reversable
	p = "games played",
	w = "games won",
	k = "players killed",
	d = "times died"
}

local team_deflation = {
	bystander = "b", b = "b",
	murderer = "m", m = "m"
}

local team_inflation = {
	b = "bystander",
	m = "murderer"
}

function murdertest.generate_stats(player)
	local stats = murdertest.util.copy_table(murdertest.default_stats)
	stats.player = player:get_player_name()
	return stats
end

function murdertest.generate_data(player)
	local data = old_generate_data(player)
	data.stats = murdertest.stats[player:get_player_name()] or murdertest.generate_stats(player)
	for i, v in pairs(murdertest.default_stats) do
		if type(v) == "function" then
			data.stats[i] = v
		end
	end
	murdertest.stats[player:get_player_name()] = data.stats
	return data
end

function murdertest.callbacks.on_shutdown(...) -- ... is used to futureproof if any args are added
	return old_on_shutdown(...), murdertest.save_stats()
end

murdertest.hud.stats = {
	background = {
		hud_elem_type = "image",
		position = {x = 1, y = 0.5},
		offset = {x = -75, y = -55},
		text = "stats_background.png",
		scale = {x = 150, y = 110}
	},
	title = {
		hud_elem_type = "text",
		position = {x = 1, y = 0.5},
		offset = {x = -75, y = -95},
		text = "MurderTest",
		name = "Title",
		alignment = 0,
		scale = {x = 200, y = 30},
		number = 0xFF0000
	},
	wins = {
		hud_elem_type = "text",
		position = {x = 1, y = 0.5},
		offset = {x = -75, y = -65},
		text = "Wins: 0",
		name = "Wins",
		alignment = 0,
		scale = {x = 200, y = 20},
		number = 0xFFFFFF
	},
	kills = {
		hud_elem_type = "text",
		position = {x = 1, y = 0.5},
		offset = {x = -75, y = -40},
		text = "Kills: 0",
		name = "Kills",
		alignment = 0,
		scale = {x = 200, y = 20},
		number = 0xFFFFFF
	},
	deaths = {
		hud_elem_type = "text",
		position = {x = 1, y = 0.5},
		offset = {x = -75, y = -15},
		text = "Deaths: 0",
		name = "Deaths",
		alignment = 0,
		scale = {x = 200, y = 20},
		number = 0xFFFFFF
	}
}

murdertest.load_stats()
