# Issue Description
Please be as descriptive as possible.
Make sure to include one of the following:
- Exact steps to reproduce a bug,
- How an enhancement could benefit MurderTest,
- How a proposal could be efficiently implemented,
- Parts of the API that a task can use.
This will help an issue to be solved faster.

# Issue Kind
- Bug: A fault in any aspect of MurderTest.
- Enhancement: An extension of an implemented or planned feature to improve it.
- Proposal: A feature that you want to be added but is not yet planned.
- Task: A planned feature that is to be added soon.

# Issue Priority
- Trivial: A fault in a non-gameplay element that does not stop it from working. (e.g. a typo)
- Low Priority: A fault in a non-gameplay element that stops it from working. (e.g. not having a command available)
- High Priority: A fault in a gameplay element that does not it from working. (e.g. knife not throwing)
- Critical: A fault in a gameplay element or the game itself that stops it from working. (e.g. weapons not spawning, crashes)
